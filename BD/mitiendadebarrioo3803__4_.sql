-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-09-2022 a las 05:04:54
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mitiendadebarrioo3803`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nom_producto` varchar(100) NOT NULL,
  `presentacion` varchar(255) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `precio_venta` decimal(10,0) NOT NULL DEFAULT 0,
  `fecha_create` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nom_producto`, `presentacion`, `id_categoria`, `precio_venta`, `fecha_create`) VALUES
(0, 'Pan Fresco La Casa del Pan ', 'Unidad de 300 gr', 6, '500', '2022-09-11'),
(1, 'Leche Colanta', 'Bolsa 1 litro', 5, '5000', '2022-09-09'),
(2, 'Vino Gato Negro', 'Botella 750 ml', 2, '45000', '2022-09-09'),
(3, 'Aguardiente Antioqueño', 'Botella 750 ml', 2, '40000', '2022-09-11'),
(4, 'Aguardiente Antioqueño ', 'Botella 1 Litro', 2, '50000', '2022-09-11'),
(5, 'Aguardiente Antioqueño', 'Botella 375 ml', 2, '25000', '2022-09-11'),
(6, 'Aguardiente Antioqueño ', 'Botella 1750 ml', 2, '78000', '2022-09-11'),
(7, 'Ron Medellin', 'Botella 375 ml', 2, '280000', '2022-09-11'),
(8, 'Ron Medellin', 'Botella 750 ml', 2, '52000', '2022-09-11'),
(9, 'Ron Medellin', 'Botella 1 litro', 2, '64000', '2022-09-11'),
(10, 'Ron Medellin', 'Botella 1750 ml', 2, '95000', '2022-09-11'),
(11, 'Ron Viejo de Caldas', 'Botella 375 ml', 2, '27000', '2022-09-11'),
(12, 'Ron Viejo de Caldas', 'Botella 750 ml', 2, '50000', '2022-09-11'),
(13, 'Ron Viejo de Caldas', 'Botella 1 Litro', 2, '81000', '2022-09-11'),
(14, 'Ron Viejo de Caldas', 'Botella 1750 ml', 2, '102000', '2022-09-11'),
(15, 'Vino Casillero del Diablo', 'Botella 750 ml ', 2, '45000', '2022-09-11'),
(16, 'Vino J.P. Chenet', 'Botella 750 ml ', 2, '55000', '2022-09-11'),
(17, 'Vino Castillo de Molina', 'Botella 750 ml', 2, '54000', '2022-09-11'),
(18, 'Vino Santa Carolina', 'Botella 750 ml', 2, '64000', '2022-09-11'),
(19, 'Vino de Misa Juan Palo II', 'Botella 750 ml', 2, '27000', '2022-09-11'),
(20, 'Vino Rosé Lambrusco', 'Botella 750 ml', 2, '87000', '2022-09-11'),
(21, 'Vino Pinta Cabras', 'Botella 750 ml', 2, '24000', '2022-09-11'),
(22, 'Vodka Smirnoff', 'Botella 750 ml', 2, '55000', '2022-09-11'),
(23, 'Vodka Absolute', 'Botella 750 ml', 2, '58000', '2022-09-11'),
(24, 'Tequila Don Julio ', 'Botella 750 ml', 2, '180000', '2022-09-11'),
(25, 'Tequila 1800', 'Botella 750 ml', 2, '155000', '2022-09-11'),
(26, 'Botella Jose Cuervo', 'Botella 750 ml ', 2, '75000', '2022-09-11'),
(27, 'Whisky Buccanans', 'Botella 750 ml', 2, '120000', '2022-09-11'),
(28, 'Tequila Jose Cuervo', 'Botella 750 ml', 2, '75000', '2022-09-11'),
(29, 'Tequila Patrón', 'Botella 750 ml', 2, '220000', '2022-09-04'),
(30, 'Cerveza Club Colombia', 'Botella 330 ml', 2, '2800', '2022-09-11'),
(31, 'Cerveza Aguila ', 'Botella 330 ml', 2, '2500', '2022-09-11'),
(32, 'Cerveza Póker', 'Botella 330 ml', 2, '2600', '2022-09-11'),
(33, 'Cerveza Costeña', 'Botella 330 ml', 2, '2500', '2022-09-11'),
(34, 'Cerveza Pilsen ', 'Botella 330 ml', 2, '2500', '2022-09-11'),
(35, 'Cerveza Heineken', 'Botella 330 ml', 2, '3000', '2022-09-11'),
(36, 'Cerveza Stella ', 'Botella 330 ml', 2, '2800', '2022-09-11'),
(37, 'Cerveza Corona', 'Botella 330 ml', 2, '3200', '2022-09-11'),
(38, 'Cerveza Aguila Light', 'Botella 330 ml', 2, '2400', '2022-09-11'),
(39, 'Cerveza Polar', 'Botella 330 ml', 2, '2500', '2022-09-11'),
(40, 'Leche Alqueria', 'Bolsa 1 Litro', 5, '3000', '2022-09-11'),
(41, 'Leche Alpina', 'Bolsa 1 Litro', 5, '3200', '2022-09-11'),
(42, 'Queso Sabana Alpina', 'Bolsa 500 gr', 5, '11000', '2022-09-11'),
(43, 'Queso Colanta', 'Bolsa 500 gr', 5, '7000', '2022-09-11'),
(44, 'Cuajada Colanta', 'Bolsa 500 gr', 5, '7500', '2022-09-11'),
(45, 'Quesito Colanta', 'Bolsa 500 gr', 5, '6000', '2022-09-11'),
(46, 'Queso Holandes', 'Bolsa 500 gr', 5, '25000', '2022-09-11'),
(47, 'Huevos Avinal', 'Canasta 30', 5, '17000', '2022-09-11'),
(48, 'Huevos Avinal', 'Canasta 12', 5, '7500', '2022-09-11'),
(49, 'Yogurt Alpina', 'Vaso 250 ml', 5, '4000', '2022-09-11'),
(50, 'Kumis Alpina', 'Vaso 250 ml', 5, '4000', '2022-09-11'),
(51, 'Yogurt Griego Alpina', 'Vaso 150 gr', 5, '5000', '2022-09-11'),
(52, 'Mantequilla Alpina', 'Caja 500 gr', 5, '11000', '2022-09-11'),
(53, 'Margarina La Fina', 'Caja 500 gr', 5, '6000', '2022-09-11'),
(54, 'Margarina Rama', 'Caja 500 gr', 5, '9000', '2022-09-11'),
(55, 'Arepas tela La Típica', 'Paquete 10 unidades', 5, '27000', '2022-09-11'),
(56, 'Pancakes Quaker', 'Bolsa 500 gr', 1, '8500', '2022-09-11'),
(57, 'Mani con pasas', 'Bolsa 200 gr', 7, '4700', '2022-09-11'),
(58, 'Milo Nestlé', 'Bolsa 200 gr', 1, '8000', '2022-09-11'),
(59, 'Galletas de Vainilla', 'Paquete 24 unidades', 7, '6700', '2022-09-11'),
(60, 'Galletas Oreo', 'Paquete 12 unidades', 7, '11000', '2022-09-11'),
(61, 'Choco Krispis Kellogs', 'Caja 630 gr', 7, '21000', '2022-09-11'),
(62, 'Zucaritas Kellogs', 'Caja 680 gr', 7, '19600', '2022-09-11'),
(63, 'Tostadas Bimbo', 'Paquete 14 unidades', 6, '5000', '2022-09-11'),
(64, 'Pan Perro', 'Paquete 6 unidades ', 6, '3300', '2022-09-11'),
(65, 'Galletas Saltín', 'Paquetaco 3 tacos', 7, '6500', '2022-09-11'),
(66, 'Galletas Tosh', 'Paquetes 12 unidades', 7, '6700', '2022-09-11'),
(67, 'Galletas Ducales', 'Paquetaco 4 tacos', 7, '8000', '2022-09-11'),
(68, 'Minicroissant Perman', 'Bolsa 20 unidades', 6, '6500', '2022-09-11'),
(69, 'Bizcochos Sebas-Sebas', 'Paquete 20 unidades', 6, '5500', '2022-09-11'),
(70, 'Salsa de Piña', 'Bolsa 200 gr', 1, '3700', '2022-09-11'),
(71, 'Mani Manitoba', 'Bolsa 400 gr', 7, '12400', '2022-09-11'),
(72, 'Bizcochuelos Perman', 'Paquete 12 unidades', 7, '4800', '2022-09-11'),
(73, 'Galletas Integrales Noel', 'Paquetaco 2 tacos', 7, '4400', '2022-09-11'),
(74, 'Palitos Tronquitos Perman', 'Paquete 130 gr', 7, '3000', '2022-09-11'),
(75, 'Atún Van Camps en aceite', 'Lata 160 gr', 1, '7000', '2022-09-04'),
(76, 'Tostadas Integrales Mama Inés', 'Paquete 20 unidades', 6, '6300', '2022-09-11'),
(77, 'Megalonchis FritoLay', 'Bolsa 24 unidades', 7, '18200', '2022-09-11'),
(78, 'Maizitos Fritolay', 'Bolsa 12 unidades', 7, '8500', '2022-09-11'),
(79, 'Vive 100 ', 'Botella 380 ml', 2, '2200', '2022-09-11'),
(80, 'Frescos Clight', 'Bolsa 2 unidades', 2, '1600', '2022-09-11'),
(81, 'Maní Moto', 'Bolsa 180 gr', 7, '3800', '2022-09-11'),
(82, 'Arroz Castellano', 'Bolsa 2500 gr', 1, '18600', '2022-09-11'),
(83, 'Arroz Castellano ', 'Bolsa 1000 gr', 1, '8000', '2022-09-11'),
(84, 'Arroz Castellano ', 'Bolsa 500 gr', 1, '4500', '2022-09-11'),
(85, 'Arroz Diana ', 'Bolsa 2500 gr', 1, '17000', '2022-09-11'),
(86, 'Arroz Diana', 'Bolsa 1000 gr', 1, '7000', '2022-09-11'),
(87, 'Arroz Diana ', 'Bolsa 500 gr', 1, '4000', '2022-09-11'),
(88, 'Arroz Roa', 'Bolsa 2500 gr', 1, '17500', '2022-09-11'),
(89, 'Arroz Roa', 'Bolsa 1000 gr', 1, '9000', '2022-09-11'),
(90, 'Arroz Roa', 'Bolsa 500 gr', 1, '4800', '2022-09-11'),
(91, 'Salchicha Ranchera', 'Paquete 480 gr', 5, '15900', '2022-09-11'),
(92, 'Salchicha Perro', 'Paquete 480 gr', 5, '10600', '2022-09-11'),
(93, 'Jamón Pietran Zenú', 'Paquete 500 gr', 5, '12000', '2022-09-11'),
(94, 'Jamón Pietran ', 'Paquete 250 gr', 5, '7000', '2022-09-11'),
(95, 'Jamón Standart', 'Paquete 500 gr', 5, '9000', '2022-09-11'),
(96, 'Jamón Standar', 'Paquete 250 gr', 5, '5000', '2022-09-11'),
(97, 'Tortllas Bimbo', 'Paquete 10 unidades', 5, '6000', '2022-09-11'),
(98, 'Azúcar Incauca', 'Bolsa 500 gr', 1, '6000', '2022-09-11'),
(99, 'Azúcar Incauca', 'Bolsa 500 gr', 1, '4000', '2022-09-11'),
(100, 'Azúcar Incauca', 'Bolsa 2500 gr', 1, '12000', '2022-09-11'),
(101, 'Azúcar Riopaila', 'Bolsa 500 gr ', 1, '3500', '2022-09-11'),
(102, 'Azúcar Riopaila ', 'Bolsa 1000 gr', 1, '5000', '2022-09-11'),
(103, 'Azúcar Riopaila', 'Bolsa 2500 gr', 1, '9000', '2022-09-11'),
(104, 'Frijol Cargamanto Aburrá', 'Bolsa 1000 gr', 1, '8000', '2022-09-11'),
(105, 'Frijol Cargamanto Aburrá', 'Bolsa 500 gr', 1, '4500', '2022-09-04'),
(106, 'Lentejas Aburrá', 'Bolsa 500 gr', 1, '2500', '2022-09-11'),
(107, 'Lentejas Aburrá', 'Bolsa 1000 gr', 1, '4500', '2022-09-11'),
(108, 'Garbanzos Aburrá', 'Bolsa 500 gr', 1, '3000', '2022-09-04'),
(109, 'Garbanzos Aburrá', 'Bolsa 1000 gr', 1, '5500', '2022-09-11'),
(110, 'Sal Refisal', 'Bolsa 1000 gr', 1, '4000', '2022-09-11'),
(111, 'Sal Refisal', 'Bolsa 500 gr', 1, '2500', '2022-09-11'),
(112, 'Sal Refisal ', 'Bolsa 250 gr', 1, '1700', '2022-09-11'),
(113, 'Harina de Trigo Haz de Oros', 'Bolsa 500 gr', 1, '4500', '2022-09-11'),
(114, 'Harina de Trigo Haz de Oros', 'Bolsa 1000 gr', 1, '8500', '2022-09-11'),
(115, 'Alverja Aburrá', 'Bolsa 500 gr', 1, '2800', '2022-09-11'),
(116, 'Alverja Aburrá', 'Bolsa 1000 gr', 1, '5000', '2022-09-11'),
(117, 'Quinua Aburrá', 'Bolsa 500 gr', 1, '7000', '2022-09-11'),
(118, 'Quinua Aburrá', 'Bolsa 1000 gr', 1, '13000', '2022-09-11'),
(119, 'Aceite vegetal Premier', 'Botella 3000 ml', 1, '54000', '2022-09-11'),
(120, 'Aceite Premier ', 'Botella 1000 ml', 1, '20000', '2022-09-11'),
(121, 'Aceite Premier ', 'Botella 500 ml', 1, '11000', '2022-09-11'),
(122, 'Aceite Premier ', 'Botella 250 ml', 1, '10000', '2022-09-11'),
(123, 'Aceite Oleocali', 'Botella 3000 ml ', 1, '30000', '2022-09-11'),
(124, 'Aceite Oleocali', 'Botella 1000 ml', 1, '11000', '2022-09-11'),
(125, 'Aceite Oleocali', 'Botella 500 ml', 1, '6000', '2022-09-11'),
(126, 'Aceite Oleocali', 'Botella 250 ml', 1, '3500', '2022-09-11'),
(127, 'Aceite de Oliva ', 'Botella 750 ml', 1, '37000', '2022-09-11'),
(128, 'Aceite de Oliva Gourmet', 'Botella 500 ml ', 1, '13500', '2022-09-11'),
(129, 'Aceite de Oliva Gourmet', 'Botella 250 ml', 1, '14000', '2022-09-11'),
(130, 'Mayonesa Fruco', 'Bolsa 380 gr', 1, '7000', '2022-09-11'),
(131, 'Mayonesa Fruco', 'Bolsa 200 gr', 1, '4000', '2022-09-11'),
(132, 'Salsa de tomate Fruco', 'Bolsa 1000 gr', 1, '17000', '2022-09-11'),
(133, 'Salsa de Tomate Fruco ', 'Bolsa de 500 mg', 1, '9000', '2022-09-11'),
(134, 'Salsa de tomate Fruco', 'Bolsa 250 gr', 1, '5000', '2022-09-11'),
(135, 'Mostaza Fruco', 'Bolsa 380 gr', 1, '5000', '2022-09-11'),
(136, 'Mostaza Fruco ', 'Bolsa 200 gr', 1, '3000', '2022-09-11'),
(137, 'Chocolate Luker', 'Paquete 500 gr', 1, '8000', '2022-09-11'),
(138, 'Chocolate Luker', 'Bolsa 250 gr', 1, '4500', '2022-09-11'),
(139, 'Chocolate Luker', 'Paquete 125 gr', 1, '3000', '2022-09-11'),
(140, 'Chocolate Cruz', 'Paquete 500 gr', 1, '7500', '2022-09-11'),
(141, 'Chocolate Cruz ', 'Paquete 250 gr', 1, '4000', '2022-09-11'),
(142, 'Chocolate Cruz', 'Paquete 125 gr', 1, '2800', '2022-09-11'),
(143, 'Café la Bastilla', 'Bolsa 500 gr ', 1, '5000', '2022-09-11'),
(144, 'Café la Bastilla', 'Bolsa 250 gr', 1, '3000', '2022-09-11'),
(145, 'Café La Bastilla ', 'Bolsa 125 gr', 1, '1500', '2022-09-11'),
(146, 'Café Colcafé ', 'Frasco 400 gr', 1, '8000', '2022-09-11'),
(147, 'Café Colcafe', 'Frasco 800 gr', 1, '15000', '2022-09-11'),
(148, 'Café Colcafé', 'Frasco 200 gr', 1, '3000', '2022-09-11'),
(149, 'Café Nescafé', 'Frasco 800 gr', 1, '9000', '2022-09-11'),
(150, 'Café Nescafé', 'Frasco 400 gr', 1, '5000', '2022-09-11'),
(151, 'Café Nescafé 200 gr', 'Frasco 200 gr', 1, '3000', '2022-09-11'),
(152, 'Pastas Doria variadas', 'Paquete 250 gr', 1, '3500', '2022-09-11'),
(153, 'Pasta Spaguetti Doria', 'Paquete 250 gr', 1, '3500', '2022-09-11'),
(154, 'Pastas Variadas Doria', 'Paquete 500 mg', 1, '6000', '2022-09-11'),
(155, 'Pastas Spaguetti Doria', 'Paquete 500 mg', 1, '6000', '2022-09-11'),
(156, 'Carne de Res Solomito', 'Paquete 500 gr', 4, '20000', '2022-09-11'),
(157, 'Carne de res Solomo redondo', 'Paquete 500 gr', 4, '12000', '2022-09-11'),
(158, 'Carne de Res Tabla', 'Paquete 500 gr', 4, '12000', '2022-09-11'),
(159, 'Carne de Res Punta de Anca', 'Paquete 500 gr', 4, '18000', '2022-09-11'),
(160, 'Carne de Res Solomo Extenjero', 'Paquete 500 gr', 4, '18000', '2022-09-11'),
(161, 'Carne de res Posta', 'Paquete 500 gr', 4, '12000', '2022-09-11'),
(162, 'Carne de res Muchacho', 'Paquete 500 mg', 4, '12000', '2022-09-11'),
(163, 'Carne de res Punta de Falda', 'Paquete 500 mg', 4, '8000', '2022-09-11'),
(164, 'Carne de res Pecho', 'Paquete 500 mg', 4, '8000', '2022-09-11'),
(165, 'Carne de res Tres Telas', 'Paquete 500 gr', 4, '8000', '2022-09-11'),
(166, 'Carne res Higado', 'Paquete 500 mg', 4, '6000', '2022-09-11'),
(167, 'Carne de res Costilla', 'Paquete 500 gr', 4, '6000', '2022-09-11'),
(168, 'Carne de res Morrillo', 'Paquete 500 gr', 4, '8000', '2022-09-11'),
(169, 'Carne de cerdo Cañón', 'Paquete de 500 gr', 4, '10000', '2022-09-11'),
(170, 'Carne de cerdo Pierna', 'Paquete 500 gr', 4, '8000', '2022-09-11'),
(171, 'Costilla de cerdo', 'Paquete de 500 mg', 4, '6000', '2022-09-11'),
(172, 'Carne de cerdo Tocino', 'Paquete 500 gr', 4, '8000', '2022-09-11'),
(173, 'Costilla de cerdo', 'Paquete 500 gr', 4, '8000', '2022-09-11'),
(174, 'Carne de Pollo Pechuga', 'Paquete 500 gr', 4, '8000', '2022-09-11'),
(175, 'Carne de Pollo Perniles', 'Paquete 500 gr', 4, '6000', '2022-09-11'),
(176, 'carne de Pollo Contramuslos', 'Paquete 500 gr', 4, '6000', '2022-09-11'),
(177, 'Carne de Pollo Alas', 'Paquete 500 gr', 4, '4000', '2022-09-11'),
(178, 'Carne de Pollo Menudencias', 'Paquete 500 gr', 4, '4000', '2022-09-11'),
(179, 'Pescado Bagre', 'Paquete 500 gr', 4, '24000', '2022-09-11'),
(180, 'Pescado Tilapia', 'Paquete 500 gr', 4, '5000', '2022-09-11'),
(182, 'Pescado Salmón', 'Paquete 500 gr', 4, '28000', '2022-09-11'),
(183, 'Pescado Sierra', 'Paquete 500 gr', 4, '6000', '2022-09-11'),
(184, 'Pescado Bassa', 'Paquete 500 gr', 4, '5000', '2022-09-11'),
(185, 'Mariscos Camaron', 'Paquete 500 gr', 4, '17000', '2022-09-11'),
(186, 'Mariscos Langostinos', 'Paquete 500 gr', 4, '34000', '2022-09-11'),
(187, 'Mariscos Pulpo', 'Paquete 500 gr', 4, '52000', '2022-09-11'),
(188, 'Mariscos Palitos de Cangrejo', 'Paquete 500 gr', 4, '12000', '2022-09-11'),
(189, 'Mariscos Langosta', 'Paquete 500 gr', 4, '70000', '2022-09-11'),
(190, 'Mariscos Ostras', 'Paquete 500 gr', 4, '18000', '2022-09-11'),
(191, 'Pan Tajado Bimbo', 'Paquete 500 gr', 6, '8000', '2022-09-11'),
(192, 'Pan Tajado Mama Inés', 'Paquete 500 gr', 6, '6000', '2022-09-11'),
(193, 'Pan de Hamburguesa Bimbo', 'Paquete 4 unidades', 6, '3500', '2022-09-11'),
(194, 'Pan Aliñado Deli', 'Unidad 500 gr', 6, '10000', '2022-09-11'),
(195, 'Pan Aliñado Deli', 'Unidad 250 gr', 6, '5500', '2022-09-11'),
(196, 'Pan Fresco La Casa del Pan', 'unidad de 100 gr', 6, '200', '2022-09-11'),
(197, 'Pan Fresco La casa del Pan', 'Unidad de 200 gr', 6, '350', '2022-09-11'),
(199, 'Pan Fresco la Casa del Pan', 'Unidad de 300 gr', 6, '500', '2022-09-11'),
(200, 'Pan con Queso La Casa del Pan', 'Unidad de 250 gr', 6, '5000', '2022-09-11'),
(201, 'Pandequeso La Casa del Pan', 'Unidad de 100 gr', 6, '4000', '2022-09-11'),
(202, 'Almojabana La casa del Pan', 'Unidad de 100 gr', 6, '5000', '2022-09-11'),
(203, 'Pan de Bono La Casa del Pan', 'Unidad de 100 gr', 6, '4000', '2022-09-11'),
(204, 'Pan Croissant La Casa del Pan', 'Unidad de 200 gr', 6, '3500', '2022-09-11'),
(205, 'Galletas de Mantequilla La Casa del Pan', 'Unidad de 50 gr', 6, '800', '2022-09-11'),
(206, 'Pastel de Guayaba', 'Unidad de 200 gr', 6, '2500', '2022-09-11'),
(207, 'Pastel de arequipe', 'Unidad de 200 gr', 6, '3500', '2022-09-11'),
(208, 'Pastel de Queso La Casa del Pan', 'Unidad de 200 gr', 6, '3500', '2022-09-11'),
(209, 'Palito de queso La Casa del Pan', 'Unidad de 200 gr', 6, '3500', '2022-09-11'),
(210, 'Porción de Torta casera la casa del Pan', 'Unidad de 250 gr', 6, '4000', '2022-09-11'),
(211, 'Bebida Coca Cola ', 'Botella de 250 ml', 2, '2500', '2022-09-11'),
(212, 'Bebida Coca Cola ', 'Unida de 350 ml', 2, '3000', '2022-09-11'),
(213, 'Bebida Coca Cola ', 'Unidad 1000 ml', 2, '5000', '2022-09-11'),
(214, 'Bebida Coca Cola ', 'Unidad de 1500 ml', 2, '6000', '2022-09-11'),
(215, 'Bebida oca Cola ', 'Unidad de 3000 ml', 2, '8000', '2022-09-11'),
(216, 'Bebida gaseosa Postobón', 'Unidad de 250 ml', 2, '2500', '2022-09-11'),
(217, 'Bebida Gaseosa Postobón', 'Unidad de 350 ml', 2, '3500', '2022-09-11'),
(218, 'Bebida gaseosa Postobón', 'Unidad de 1000 ml', 2, '5000', '2022-09-11'),
(219, 'Bebida gaseosa Postobón', 'Unidad de 1500 ml', 2, '6000', '2022-09-11'),
(220, 'Bebida gaseosa Postobón', 'Unidad de 3000 ml', 2, '8000', '2022-09-11'),
(221, 'Bebida gaseosa Kola Roman', 'Unidad de 350 ml', 2, '3000', '2022-09-11'),
(222, 'Bebida gaseosa Kola Roman', 'Unidad de 1000 ml', 2, '5000', '2022-09-11'),
(223, 'Bebida gaseosa Kola Roman', 'Unidad de 1500 ml', 2, '6000', '2022-09-11'),
(224, 'Bebida gaseosa Kola Roman ', 'Unidad por 3000 ml', 2, '8000', '2022-09-11'),
(225, 'CHICLE TRIDENT 10/16G', 'Paquete 10/16G', 7, '3000', '2022-09-11'),
(226, 'CHCLE BUBBALOO ', 'Unidad 10 gr', 7, '200', '2022-09-11'),
(227, 'Chicles Adams', 'Caja por 2 unidades', 7, '300', '2022-09-11'),
(228, 'Chicles Adams', 'Caja 10 unidades', 7, '1000', '2022-09-11'),
(229, 'Dulces Bombón bum', 'Unidad 50 gr', 7, '500', '2022-09-11'),
(230, 'Dulces Cofee Delight', 'Unidad 20 gr', 7, '100', '2022-09-11'),
(231, 'Dulces Barrilete', 'Unidad 25 gr', 7, '200', '2022-09-11'),
(232, 'Dulces Frunas', 'Paquete por 4 unidades', 7, '200', '2022-09-11'),
(233, 'Snakc Cheetos', 'Paquete 15 gr', 7, '1000', '2022-09-11'),
(234, 'Snack Boliqueso', 'Paquete 20 gr', 7, '1200', '2022-09-11'),
(235, 'Papas Margarita', 'Paquete 20 gr', 7, '1500', '2022-09-11'),
(236, 'Rosquillas Margarita', 'Paquete 20 gr', 7, '1000', '2022-09-11'),
(237, 'Doritos Fritolay', 'Paquete 30 gr', 7, '2000', '2022-09-11'),
(238, 'Tozinetas Fritolay', 'Paquete 25 gr', 7, '2000', '2022-09-11'),
(239, 'Nucita Colombina', 'Unidad 30 gr', 7, '1000', '2022-09-11'),
(240, 'Lecherita Nestlé', 'Lata 100 gr', 7, '2000', '2022-09-11'),
(241, 'Lolita Ramo', 'Unidad 100 gr', 7, '1500', '2022-09-12'),
(242, 'Torta Chocolate Ramo', 'Unidad 200 gr', 7, '2000', '2022-09-12'),
(243, 'Gomas Trululu', 'Unidad 10 gr', 7, '200', '2022-09-12'),
(244, 'Galletas Chusquitas Mama Inés', 'Paquete por 5 Unidades', 7, '3500', '2022-09-12'),
(245, 'Vinagre de frutas Respin', 'Frasco por 250 ml ', 1, '2500', '2022-09-12'),
(246, 'Panela Yolombo', 'Unidad 250 gr', 1, '1000', '2022-09-12'),
(247, 'Endulzante Stevia en polvo', 'Frasco 100 gr', 1, '4500', '2022-09-12'),
(248, 'Aromáticas Hindú', 'Caja por 12 sobres', 1, '7000', '2022-09-12'),
(249, 'Queso crema Colanta ', 'Caja 250 gr', 5, '3500', '2022-09-12'),
(250, 'Sopas Maggie', 'Sobres por 100 gr', 1, '3000', '2022-09-12'),
(251, 'Condimento Ricostilla ', 'Caja por 6 unidades ', 1, '4000', '2022-09-12'),
(252, 'Caldo de Gallina Knorr', 'Caja por 6 unidades ', 1, '3500', '2022-09-12'),
(253, 'Pimienta Zafran', 'Frasco 100 gr', 1, '5000', '2022-09-12'),
(254, 'Ensaladas de Verduras en conserva Zenú', 'Enlatado 500 gr ', 1, '8000', '2022-09-12'),
(255, 'Melocotones en conserva Frugal', 'Lata por 500 gr', 1, '6000', '2022-09-12'),
(256, 'Chocololatina Jet Colombina', 'Unidad 100 gr', 7, '300', '2022-09-12'),
(257, 'Chocolatina Yumbo Colombina', 'Unidad 300 gr', 7, '2000', '2022-09-12'),
(258, 'Chocolatina Milky Way', 'Unidad 200 gr', 7, '3000', '2022-09-12'),
(259, 'Chocolatina Snickers', 'Unidad 250 gr', 7, '3500', '2022-09-12'),
(260, 'Bebida de te Hatsú', 'Botella por 350 ml', 2, '6000', '2022-09-12'),
(261, 'Chorizos de ternera', 'Paquete por 8 unidades', 5, '8000', '2022-09-12'),
(262, 'Chorizo de Cerdo', 'Paquete por 8 unidades', 5, '9600', '2022-09-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `nit` int(11) NOT NULL,
  `nom_proveedor` varchar(45) NOT NULL,
  `nom_vendedor` varchar(45) DEFAULT NULL,
  `telefono` varchar(30) NOT NULL,
  `direccion` varchar(45) DEFAULT NULL,
  `fecha_create` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `nit`, `nom_proveedor`, `nom_vendedor`, `telefono`, `direccion`, `fecha_create`) VALUES
(1, 890904525, 'Bavaria', 'Marisol Idarraga', '397853', 'Barranquilla', '2022-09-11 17:35:24'),
(2, 890903858, 'Coca Cola', 'Carlos Restrepo', '2693089', 'Bogotá', '2022-09-11 17:31:27'),
(3, 890654764, 'Colanta', 'Juan Camilo Alvarez', '5347864', 'Medellin', '2022-09-11 17:40:06'),
(4, 34567897, 'Zenú', 'Luisa Maria River', '4567893', 'Medellin', '2022-09-11 17:42:15'),
(5, 90864326, 'Santa Anita', 'Maria Jose Ramirez', '6543210', 'Bogotá', '2022-09-11 17:42:33'),
(6, 806754321, 'DistriCarnes', 'Juan Pablo Zuluaga', '75643587', 'Cali', '2022-09-11 17:44:08'),
(7, 543218976, 'Fabrica de Licores de Antioquia', 'Erick Javier Fernandez', '6532416', 'Medellin', '2022-09-11 17:47:38'),
(8, 534217865, 'DistriLicores', 'Salome Alcaraz ', '5324691', 'Barranquilla', '2022-09-11 17:48:47'),
(9, 64532198, 'Plaza Mayorista Local 805', 'Ana Carolina Giraldo', '9865341', 'Bogotá', '2022-09-11 17:50:30'),
(10, 78905643, 'Bimbo', 'Marina Legarda', '54329876', 'Bucaramanga', '2022-09-11 17:52:15'),
(11, 906543767, 'Pepsico', 'Rodrigo Angarita', '8654321', 'Cali', '2022-09-11 17:53:32'),
(12, 23497134, 'Industrias Nioel', 'Mauricio Jaramillo ', '67396412', 'Medellin', '2022-09-11 17:56:18'),
(13, 32197564, 'Mac Pollo', 'Elizabeth Jimenez', '65432908', 'Bogotá', '2022-09-11 17:58:02'),
(14, 210975643, 'Vitamar', 'Yury Mendoza', '97365321', 'Barranquilla', '2022-09-11 18:00:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_categorias`
--

CREATE TABLE `tipo_categorias` (
  `id_categoria` int(11) NOT NULL,
  `nom_categoria` varchar(45) NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_categorias`
--

INSERT INTO `tipo_categorias` (`id_categoria`, `nom_categoria`, `descripcion`) VALUES
(1, 'Despensa', 'Arroz, Azúcar, Sal, Frijol, café, Harina De trigo, Pastas, Aceites, Salsa de Tomate, Mayonesa, Gelatina, Mermelada'),
(2, 'Bebidas y Licores', 'Gaseosas, Jugos, Té, Cervezas, Whisky, Vinos, Wodka, Aguardiente, Ron, Tequila.'),
(3, 'Delicatessen', 'Quesos, Carnes especiales, Frutos secos, Jamones, Peperoni, Salamies, Anchoas, Aceitunas.'),
(4, 'Pollo, carne y pescados', 'Pollo, Carne de res, Carne de cerdo, Pescado, Mariscos.'),
(5, 'Lácteos, huevos y refrigerados', 'Leche, huevos, Derivados de los lácteos, arepas y tortillas.'),
(6, 'Panaderia y reposteria', 'Panadería fresca, Panadería empacada, repostería, Ingredientes de repostería.'),
(7, 'Snacks', 'Pasabocas, Chocolates, confitería, dulces, galletas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_tienda` int(11) NOT NULL,
  `nom_usuario` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `password` blob NOT NULL,
  `tipo_usuario` int(1) NOT NULL DEFAULT 1,
  `estado` int(1) NOT NULL DEFAULT 1,
  `fecha_create` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `fk_productos_tipo_categorias1_idx` (`id_categoria`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `tipo_categorias`
--
ALTER TABLE `tipo_categorias`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `correo_UNIQUE` (`correo`),
  ADD KEY `fk_usuario_tienda1_idx` (`id_tienda`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=263;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tipo_categorias`
--
ALTER TABLE `tipo_categorias`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_tipo_categorias1` FOREIGN KEY (`id_categoria`) REFERENCES `tipo_categorias` (`id_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_tienda1` FOREIGN KEY (`id_tienda`) REFERENCES `tienda` (`id_tienda`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
