-- MySQL Script generated by MySQL Workbench
-- Thu Sep  1 20:22:30 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema MitiendadebarrioO3803
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema MitiendadebarrioO3803
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `MitiendadebarrioO3803` DEFAULT CHARACTER SET utf8 ;
USE `MitiendadebarrioO3803` ;

-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`tienda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`tienda` (
  `id_tienda` INT NOT NULL AUTO_INCREMENT,
  `nom_tienda` VARCHAR(100) NOT NULL,
  `nit` INT NOT NULL,
  `direccion` VARCHAR(100) NOT NULL,
  `telefono` VARCHAR(30) NOT NULL,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_tienda`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`usuario` (
  `id_usuario` INT NOT NULL AUTO_INCREMENT,
  `id_tienda` INT NOT NULL,
  `nom_usuario` VARCHAR(100) NOT NULL,
  `correo` VARCHAR(100) NOT NULL,
  `password` BLOB NOT NULL,
  `tipo_usuario` INT(1) NOT NULL DEFAULT 1,
  `estado` INT(1) NOT NULL DEFAULT 1,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE INDEX `correo_UNIQUE` (`correo` ASC),
  PRIMARY KEY (`id_usuario`),
  INDEX `fk_usuario_tienda1_idx` (`id_tienda` ASC),
  CONSTRAINT `fk_usuario_tienda1`
    FOREIGN KEY (`id_tienda`)
    REFERENCES `MitiendadebarrioO3803`.`tienda` (`id_tienda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`tipo_categorias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`tipo_categorias` (
  `id_categoria` INT NOT NULL AUTO_INCREMENT,
  `nom_categoria` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(150) NULL,
  PRIMARY KEY (`id_categoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`productos` (
  `id_producto` INT NOT NULL AUTO_INCREMENT,
  `nom_producto` VARCHAR(100) NOT NULL,
  `presentacion` VARCHAR(255) NOT NULL,
  `id_categoria` INT NOT NULL,
  `precio_venta` DECIMAL(10) NOT NULL DEFAULT 0,
  `fecha_create` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_producto`),
  INDEX `fk_productos_tipo_categorias1_idx` (`id_categoria` ASC),
  CONSTRAINT `fk_productos_tipo_categorias1`
    FOREIGN KEY (`id_categoria`)
    REFERENCES `MitiendadebarrioO3803`.`tipo_categorias` (`id_categoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`proveedores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`proveedores` (
  `id_proveedor` INT NOT NULL AUTO_INCREMENT,
  `nit` INT NOT NULL,
  `nom_proveedor` VARCHAR(45) NOT NULL,
  `nom_vendedor` VARCHAR(45) NULL,
  `telefono` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(45) NULL,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_proveedor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`compras`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`compras` (
  `id_compra` INT NOT NULL AUTO_INCREMENT,
  `id_tienda` INT NOT NULL,
  `id_proveedor` INT NOT NULL,
  `nro_factura` VARCHAR(45) NOT NULL,
  `valor_total` DECIMAL(20) NOT NULL DEFAULT 0,
  `valor_iva` DECIMAL(10) NOT NULL DEFAULT 0,
  `valor_subtotal` DECIMAL(20) NOT NULL DEFAULT 0,
  `fecha_compra` DATETIME NOT NULL,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_compra`),
  INDEX `fk_compras_proveedores1_idx` (`id_proveedor` ASC),
  INDEX `fk_compras_tienda1_idx` (`id_tienda` ASC),
  CONSTRAINT `fk_compras_proveedores1`
    FOREIGN KEY (`id_proveedor`)
    REFERENCES `MitiendadebarrioO3803`.`proveedores` (`id_proveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compras_tienda1`
    FOREIGN KEY (`id_tienda`)
    REFERENCES `MitiendadebarrioO3803`.`tienda` (`id_tienda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`inventarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`inventarios` (
  `id_inventario` INT NOT NULL AUTO_INCREMENT,
  `id_tienda` INT NOT NULL,
  `cantidad` INT NOT NULL DEFAULT 0,
  `limite_notifica` INT NOT NULL DEFAULT 0,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_update` DATETIME NULL,
  PRIMARY KEY (`id_inventario`),
  INDEX `fk_inventarios_tienda1_idx` (`id_tienda` ASC),
  CONSTRAINT `fk_inventarios_tienda1`
    FOREIGN KEY (`id_tienda`)
    REFERENCES `MitiendadebarrioO3803`.`tienda` (`id_tienda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`ventas` (
  `id_venta` INT NOT NULL AUTO_INCREMENT,
  `id_tienda` INT NOT NULL,
  `valor_total` DECIMAL(10) NOT NULL DEFAULT 0,
  `fecha_venta` DATETIME NOT NULL,
  `nom_cliente` VARCHAR(45) NULL,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_venta`),
  INDEX `fk_ventas_tienda1_idx` (`id_tienda` ASC),
  CONSTRAINT `fk_ventas_tienda1`
    FOREIGN KEY (`id_tienda`)
    REFERENCES `MitiendadebarrioO3803`.`tienda` (`id_tienda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`compras_productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`compras_productos` (
  `id_compra` INT NOT NULL,
  `id_producto` INT NOT NULL,
  `cantidad` DECIMAL(10,2) NULL,
  `valor_unitario` DECIMAL(10,2) NULL,
  `sub_total` DECIMAL(10,2) NULL,
  `valor_iva` DECIMAL(10,2) NULL,
  `valor_total` DECIMAL(10,2) NULL,
  `fecha_compra` DATETIME NOT NULL,
  `fecha_create` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_compra`, `id_producto`),
  INDEX `fk_compras_has_productos_productos1_idx` (`id_producto` ASC),
  INDEX `fk_compras_has_productos_compras1_idx` (`id_compra` ASC),
  CONSTRAINT `fk_compras_has_productos_compras1`
    FOREIGN KEY (`id_compra`)
    REFERENCES `MitiendadebarrioO3803`.`compras` (`id_compra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compras_has_productos_productos1`
    FOREIGN KEY (`id_producto`)
    REFERENCES `MitiendadebarrioO3803`.`productos` (`id_producto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MitiendadebarrioO3803`.`productos_ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `MitiendadebarrioO3803`.`productos_ventas` (
  `id_producto` INT NOT NULL,
  `id_venta` INT NOT NULL,
  `nom_cliente` VARCHAR(100) NULL,
  `cantidad` INT(5) NOT NULL DEFAULT 0,
  `valor_unitario` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `valor_total` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `precio_compra` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `fecha_venta` DATETIME NOT NULL,
  `fecha_create` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_producto`, `id_venta`),
  INDEX `fk_productos_has_ventas_ventas1_idx` (`id_venta` ASC),
  INDEX `fk_productos_has_ventas_productos1_idx` (`id_producto` ASC),
  CONSTRAINT `fk_productos_has_ventas_productos1`
    FOREIGN KEY (`id_producto`)
    REFERENCES `MitiendadebarrioO3803`.`productos` (`id_producto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_ventas_ventas1`
    FOREIGN KEY (`id_venta`)
    REFERENCES `MitiendadebarrioO3803`.`ventas` (`id_venta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
