/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


function loadData() {
    let request = sendRequest('tiposcategoria/list', 'GET', '');
    request.onload = function() {
        let data = request.response;
        let tabla = document.getElementById('tabla-categorias');
         tabla.innerHTML = '';
         data.forEach( element => tabla.innerHTML += (`
         <tr>
            <th scope="row">${element.idCategoria}</th>
            <td>${element.nombreCategoria}
                <br/> <small>${element.descripcion}</small>
            </td>
            <td class="align-middle" style="width: 190px">
                <div class="d-flex justify-content-evenly ">
                    <a class="btn btn-outline-primary btn-sm" href="form_categorias.html?id=${element.idCategoria}">Editar</a>
                    <button class="btn btn-outline-danger btn-sm" type="button" onclick="deleteItem('${element.idCategoria}')">Eliminar</button>
                </div>
            </td>
        </tr>
        `));
    };
}

function loadItem(id) {
    let request = sendRequest('tiposcategoria/list/'+id, 'GET', '');
    let elId = document.getElementById('id');
    let nombre = document.getElementById('nombre');
    let descripcion = document.getElementById('descripcion');
    request.onload = function() {
        let data = request.response;
        elId.value = data.idCategoria;
        nombre.value = data.nombreCategoria;
        descripcion.value = data.descripcion;   
    };
    request.onerror = function() {
        alert('Error al consultar datos');
    };
}

function saveItem() {
    let elId = document.getElementById('id');
    let nombre = document.getElementById('nombre');
    let descripcion = document.getElementById('descripcion');
    let data = {
        idCategoria: elId.value,
        nombreCategoria: nombre.value,
        descripcion: descripcion.value
    };
    let request = sendRequest('tiposcategoria/', elId.value ? 'PUT' : 'POST', data);
    request.onload = function(){
        window.location = 'categorias.html';
    };
    request.onerror = function(){
        alert('Error al guardar los datos');
    };
}

function deleteItem(id) {
    let request = sendRequest('tiposcategoria/' + id, 'DELETE', '');
    request.onload = function(){
        loadData();
    };
}