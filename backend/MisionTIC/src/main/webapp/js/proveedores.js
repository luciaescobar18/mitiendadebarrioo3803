function loadData(){
    let request = sendRequest('proveedores/list', 'GET', '')
    let table = document.getElementById('proveedores-table');
    request.onload= function(){
        table.innerHTML="";
        let data = request.response;
        console.log(data);
        data.forEach((element,index)=> {
            table.innerHTML+= `
                <tr class="table-provee">
                    <th>${element.idProveedor}</th>
                    <td>${element.nit}</td>
                    <td>${element.nombreProveedor}</td>
                    <td class="d-none d-md-table-cell">${element.nombreVendedor}</td>
                    <td class="d-none d-md-table-cell">${element.telefono}</td>
                    <td class="d-none d-md-table-cell">${element.direccion}</td>
                    <td class="d-none d-md-table-cell">${element.fechaCreate}</td>
                    <td class="align-middle" style="width: 190px">
                         <div class="d-flex justify-content-evenly ">
                              <a class="btn btn-outline-primary btn-sm" href="formProveedor.html?id=${element.idProveedor}">Editar</a>
                              <button class="btn btn-outline-danger btn-sm" type="button" onclick="deleteProveedor('${element.idProveedor}')">Eliminar</button>
                         </div>
                    </td>
                </tr>
                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `<tr><td colspan="6">Error al recuperar los datos.</td>`;
    }
}
function loadProveedor(idProveedor){
     let request = sendRequest('proveedores/list/'+idProveedor,'GET','')
     let id = document.getElementById('provee-id')
     let nit = document.getElementById('provee-nit')
     let name = document.getElementById('provee-name')
     let salesman = document.getElementById('provee-salesman')
     let phone = document.getElementById('provee-phone')
     let address = document.getElementById('provee-address')
     let datacreate = document.getElementById('provee-datacreate')
     request.onload = function (){
          let data = request.response
          id.value = data.idProveedor
          nit.value = data.nit
          name.value = data.nombreProveedor
          salesman.value= data.nombreVendedor
          phone.value = data.telefono
          address.value = data.direccion
          datacreate.value = data.fechaCreate
     }
     request.onerror  = function (){
          alert('Error al actualizar los datos de proveedores.')
     }
}
function deleteProveedor(idProveedor){
     let request = sendRequest('proveedores/'+idProveedor,'DELETE','')
     request.onload=function(){
          loadData()
     }
     request.onerror= function(){
          alert('Error al intentar eliminar el proveedor de id '+idProveedor)
     }
}
function saveProveedor(){
    let id = document.getElementById('provee-id').value
    let nit = document.getElementById('provee-nit').value
    let name = document.getElementById('provee-name').value
    let salesman = document.getElementById('provee-salesman').value
    let phone = document.getElementById('provee-phone').value
    let address = document.getElementById('provee-address').value
    let datacreate = document.getElementById('provee-datacreate').value
     
    let crearProveedor ={
          'idProveedor':id,
          'nit':nit,
          'nombreProveedor':name,
          'nombreVendedor':salesman ,
          'telefono':phone,
          'direccion':address,
          'fechaCreate':datacreate 
     }
     let request = sendRequest('proveedores/', id ? 'PUT' : 'POST', crearProveedor);
     request.onload = function(){
          window.location ='formProveedores.html';
     }
     request.onerror = function (){
          alert('Error al guardar el proveedor')
     }
}
