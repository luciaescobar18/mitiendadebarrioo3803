/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function loadTiposCategoria() {
    let request = sendRequest('tiposcategoria/list', 'GET', '');
    request.onload = function() {
        let data = request.response;
        let select = document.getElementById('product-category');
         select.innerHTML = '<option>Selecione categoria</option>';
         data.forEach( element => select.innerHTML += (`'<option value="${element.idCategoria}">${element.nombreCategoria}</option>';`));

        const urlParams = new URLSearchParams(window.location.search);
        if (urlParams.get('id') !== null) {
            loadProducto(urlParams.get("id"));
        }
    };
}
function loadData(){
    let request = sendRequest('productos/list', 'GET', '')
    let table = document.getElementById('productos-table');
    request.onload= function(){
        table.innerHTML="";
        let data = request.response;
        console.log(data);
        data.forEach((element,index)=> {
            table.innerHTML+= `
                <tr class="table-producto">
                    <th>${element.idProducto}</th>
                    <td>${element.nombreProducto}</td>
                    <td class="d-none d-md-table-cell">${element.presentacion}</td>
                    <td class="d-none d-md-table-cell">${element.categoria.nombreCategoria}</td>
                    <td class="d-none d-md-table-cell">${element.precioVenta}</td>
                    <td class="d-none d-md-table-cell">${element.fechaCreate}</td>
                    <td class="align-middle" style="width: 190px">
                        <div class="d-flex justify-content-evenly ">
                            <a class="btn btn-outline-primary btn-sm" href="formProducto.html?id=${element.idProducto}">Editar</a>
                            <button class="btn btn-outline-danger btn-sm" type="button" onclick="deleteProducto('${element.idProducto}')">Eliminar</button>
                        </div>
                    </td>
                </tr>
                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `<tr><td colspan="6">Error al recuperar los datos.</td>`;
    }
}
function loadProducto(idProducto){
     let request = sendRequest('productos/list/'+idProducto,'GET','') 
     let productoId = document.getElementById('product-id')
     let name = document.getElementById('product-name')
     let presentation = document.getElementById('product-present')
     let category = document.getElementById('product-category')
     let sales = document.getElementById('product-venta')
     let fecha = document.getElementById('product-datacreate')
     request.onload = function (){
          let data = request.response
          productoId.value = data.idProducto
          name.value = data.nombreProducto
          presentation.value= data.presentacion
          category.value = data.categoria.idCategoria
          sales.value = data.precioVenta
          fecha.value = data.fechaCreate
     }
     request.onerror  = function (){
          alert('Error al actualizar los datos de productos.')
     }
}
function deleteProducto(idProducto){
     let request = sendRequest('productos/'+idProducto,'DELETE','')
     request.onload=function(){
          loadData()
     }
     request.onerror= function(){
          alert('Error al intentar eliminar el producto de id '+idProducto)
     }
}
function saveProducto(){
    let productoId = document.getElementById('product-id')
    let name = document.getElementById('product-name')
    let presentation = document.getElementById('product-present')
    let category = document.getElementById('product-category')
    let sales = document.getElementById('product-venta')
    let fecha = document.getElementById('product-datacreate')
    
    
    let data ={
        'idProducto':productoId.value,
        'nombreProducto':name.value,
        'presentacion':presentation.value, 
        'categoria':{'idCategoria':category.value},
        'precioVenta':sales.value,
        'fechaCreate': fecha.value,
    }
     
    let request = sendRequest('productos/', productoId.value ? 'PUT' : 'POST', data);
     request.onload = function(){
          window.location ='formProductos.html';
     }
     request.onerror = function (){
          alert('Error al guardar el producto')
     }
}
