/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Dao;

import com.EjemploGrupo03.MisionTIC.Models.TipoCategoria;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author luciaescobar
 */
public interface TipoCategoriaDao extends CrudRepository<TipoCategoria,Integer> {
    
}
