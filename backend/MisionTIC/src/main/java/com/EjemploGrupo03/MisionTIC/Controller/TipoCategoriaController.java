/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Controller;

import com.EjemploGrupo03.MisionTIC.Models.TipoCategoria;
import com.EjemploGrupo03.MisionTIC.Service.TipoCategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luciaescobar
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/tiposcategoria")
public class TipoCategoriaController {
    @Autowired
    private TipoCategoriaService tipoCategoriaservice;
    
    @PostMapping(value="/")
    public ResponseEntity<TipoCategoria> agregar(@RequestBody TipoCategoria tipoCategoria){ 
        TipoCategoria obj = tipoCategoriaservice.save(tipoCategoria);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<TipoCategoria> eliminar(@PathVariable Integer id){
        TipoCategoria obj = tipoCategoriaservice.findById(id);
        if(obj!=null)
            tipoCategoriaservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<TipoCategoria> editar(@RequestBody TipoCategoria tipoCategoria){
        TipoCategoria obj = tipoCategoriaservice.findById(tipoCategoria.getIdCategoria());
        if(obj!=null) {
            obj.setNombreCategoria(tipoCategoria.getNombreCategoria());
            obj.setDescripcion(tipoCategoria.getDescripcion());
            tipoCategoriaservice.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<TipoCategoria> consultarTodo(){
        return tipoCategoriaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public TipoCategoria consultaPorId(@PathVariable Integer id){
        return tipoCategoriaservice.findById(id);
    }
}
