/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Service;

import com.EjemploGrupo03.MisionTIC.Models.TipoCategoria;
import java.util.List;

/**
 *
 * @author luciaescobar
 */
public interface TipoCategoriaService {
    public TipoCategoria save(TipoCategoria tipoCategoria);
    public void delete(Integer id);
    public TipoCategoria findById(Integer id);
    public List<TipoCategoria> findAll();
}
