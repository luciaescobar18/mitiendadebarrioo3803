/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Service.Implement;

import com.EjemploGrupo03.MisionTIC.Service.ProveedorService;
import com.EjemploGrupo03.MisionTIC.Dao.ProveedorDao;
import com.EjemploGrupo03.MisionTIC.Models.Proveedor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author luciaescobar
 */
@Service
public class ProveedorServiceImpl implements ProveedorService {
    @Autowired
    private ProveedorDao proveedorDao;
    
    @Override
    @Transactional(readOnly=false)
    public Proveedor save(Proveedor proveedor){ 
        return proveedorDao.save(proveedor);
    }
    
    @Override @Transactional(readOnly=false) 
    public void delete(Integer id){ 
        proveedorDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Proveedor findById(Integer id){
        return proveedorDao.findById(id).orElse(null); 
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Proveedor> findAll(){
        return (List<Proveedor>) proveedorDao.findAll(); 
    }
}
