/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Service;

import com.EjemploGrupo03.MisionTIC.Models.Proveedor;
import java.util.List;

/**
 *
 * @author luciaescobar
 */
public interface ProveedorService {
    public Proveedor save(Proveedor proveedor);
    public void delete(Integer id);
    public Proveedor findById(Integer id);
    public List<Proveedor> findAll();
}
