/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Service;

import com.EjemploGrupo03.MisionTIC.Models.Producto;
import java.util.List;

/**
 *
 * @author luciaescobar
 */
public interface ProductoService {
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
