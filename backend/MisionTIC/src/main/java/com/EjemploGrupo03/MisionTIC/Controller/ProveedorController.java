/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Controller;

import com.EjemploGrupo03.MisionTIC.Models.Proveedor;
import com.EjemploGrupo03.MisionTIC.Service.ProveedorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author luciaescobar
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/proveedores")
public class ProveedorController {
    @Autowired
    private ProveedorService proveedorservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Proveedor> agregar(@RequestBody Proveedor proveedor){ 
        Proveedor obj = proveedorservice.save(proveedor);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Proveedor> eliminar(@PathVariable Integer id){
        Proveedor obj = proveedorservice.findById(id);
        if(obj!=null)
            proveedorservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/")
    public ResponseEntity<Proveedor> editar(@RequestBody Proveedor proveedor){
        Proveedor obj = proveedorservice.findById(proveedor.getIdProveedor());
        if(obj!=null) {
            obj.setNombreProveedor(proveedor.getNombreProveedor());
            proveedorservice.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Proveedor> consultarTodo(){
        return proveedorservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Proveedor consultaPorId(@PathVariable Integer id){
        return proveedorservice.findById(id);
    }
}
