package com.EjemploGrupo03.MisionTIC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.EjemploGrupo03.MisionTIC")
public class MisionTicApplication {

	public static void main(String[] args) {
		SpringApplication.run(MisionTicApplication.class, args);
	}

}
