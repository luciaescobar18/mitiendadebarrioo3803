/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Dao;

import com.EjemploGrupo03.MisionTIC.Models.Proveedor;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author luciaescobar
 */
public interface ProveedorDao extends CrudRepository<Proveedor,Integer> {
    
}
