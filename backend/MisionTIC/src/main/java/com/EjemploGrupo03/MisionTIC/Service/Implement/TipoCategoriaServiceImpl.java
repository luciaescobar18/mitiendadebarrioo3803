/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.EjemploGrupo03.MisionTIC.Service.Implement;

import com.EjemploGrupo03.MisionTIC.Dao.TipoCategoriaDao;
import com.EjemploGrupo03.MisionTIC.Models.TipoCategoria;
import com.EjemploGrupo03.MisionTIC.Service.TipoCategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author luciaescobar
 */
@Service
public class TipoCategoriaServiceImpl implements TipoCategoriaService {
    @Autowired
    private TipoCategoriaDao tipoCategoriaDao;
    
    @Override
    @Transactional(readOnly=false)
    public TipoCategoria save(TipoCategoria tipoCategoria){ 
        return tipoCategoriaDao.save(tipoCategoria);
    }
    
    @Override @Transactional(readOnly=false) 
    public void delete(Integer id){ 
        tipoCategoriaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public TipoCategoria findById(Integer id){
        return tipoCategoriaDao.findById(id).orElse(null); 
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<TipoCategoria> findAll(){
        return (List<TipoCategoria>) tipoCategoriaDao.findAll(); 
    }
}
